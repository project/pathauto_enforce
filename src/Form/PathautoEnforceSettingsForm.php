<?php

namespace Drupal\pathauto_enforce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Pathauto Enforce settings form.
 */
class PathautoEnforceSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'pathauto_enforce.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pathauto_enforce_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form = [];

    $content_type_options = [];
    $content_type_options_not = [];
    $types = NodeType::loadMultiple();
    foreach ($types as $t) {
      $content_type_options[$t->id()] = $t->label();
      $content_type_options_not[$t->id()] = $t->label();
    }
    if ($content_type_options) {
      $form['pathauto_enforce_content_types'] = [
        '#type'          => 'checkboxes',
        '#title'         => $this->t('Enforce Pathauto patterns for these content types:'),
        '#options'       => $content_type_options,
        '#default_value' => $config->get('pathauto_enforce_content_types') ?? [],
      ];
      $form['pathauto_enforce_not_content_types'] = [
        '#type'          => 'checkboxes',
        '#title'         => $this->t('Prevent the creation and generation of aliases for these content types:'),
        '#options'       => $content_type_options_not,
        '#default_value' => $config->get('pathauto_enforce_not_content_types') ?? [],
        '#description'   => $this->t("If the 'Enforce' option is set, setting a value here will not have any effect."),
      ];
    }

    if (\Drupal::moduleHandler()->moduleExists('taxonomy')) {
      $vocabulary_options = [];
      $vocabulary_options_not = [];
      $vocabularies = Vocabulary::loadMultiple();;
      foreach ($vocabularies as $v) {
        $vocabulary_options[$v->id()] = $v->label();
        $vocabulary_options_not[$v->id()] = $v->label();
      }
      if ($vocabulary_options) {
        $form['pathauto_enforce_vocabularies'] = [
          '#type'          => 'checkboxes',
          '#title'         => $this->t('Enforce Pathauto patterns for these vocabularies:'),
          '#options'       => $vocabulary_options,
          '#default_value' => $config->get('pathauto_enforce_vocabularies') ?? [],
        ];
        $form['pathauto_enforce_not_vocabularies'] = [
          '#type'          => 'checkboxes',
          '#title'         => $this->t('Prevent the creation and generation of aliases for these vocabularies:'),
          '#options'       => $vocabulary_options_not,
          '#default_value' => $config->get('pathauto_enforce_not_vocabularies') ?? [],
          '#description'   => $this->t("If the 'Enforce' option is set, setting a value here will not have any effect."),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable(static::SETTINGS);
    $keys = [
      'pathauto_enforce_content_types',
      'pathauto_enforce_not_content_types',
      'pathauto_enforce_vocabularies',
      'pathauto_enforce_not_vocabularies',
    ];
    foreach ($keys as $key) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
